package com.tawsiq.tawsiq.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.tawsiq.tawsiq.Models.Building;
import com.tawsiq.tawsiq.Models.State;
import com.tawsiq.tawsiq.R;
import com.tawsiq.tawsiq.adapters.BuildingsAdapter;
import com.tawsiq.tawsiq.interfaces.MainActivityCallback;
import com.tawsiq.tawsiq.utils.Preferences;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment {
    MainActivityCallback callback;
    List<Building> buildings, buildingsFilter;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private BuildingsAdapter adapter;
    private Preferences preferances;
    private Spinner spinnerState, spinnerStatus, spinnerType;
    private ArrayAdapter<State> statesAdapter;
    private ArrayAdapter<String> statuserAdapter, typesAdapter;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        preferances = new Preferences(getContext());
        List<State> areaArray = new ArrayList<>();
        State state = new State();
        state.setId(0);
        state.setStateName("الكل");
        areaArray.add(state);
        areaArray.addAll(preferances.getAllStates());
        String[] statusArray = getResources().getStringArray(R.array.statuses);
        String[] typesArray = getResources().getStringArray(R.array.types);
        buildings = new ArrayList<>();
        buildingsFilter = new ArrayList<>();
        buildings.addAll(callback.getBuildings());

        spinnerType = rootView.findViewById(R.id.type);
        spinnerState = rootView.findViewById(R.id.state);
        spinnerStatus = rootView.findViewById(R.id.status);

        statesAdapter = new ArrayAdapter<State>(getActivity(), android.R.layout.simple_list_item_1, areaArray);
        spinnerState.setAdapter(statesAdapter);

        statuserAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, statusArray);
        spinnerStatus.setAdapter(statuserAdapter);

        typesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, typesArray);
        spinnerType.setAdapter(typesAdapter);

        rootView.findViewById(R.id.search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buildingsFilter.clear();
                buildingsFilter.addAll(buildings);
                filterState();
            }
        });
        recyclerView = rootView.findViewById(R.id.recycler_view);
        adapter = new BuildingsAdapter(getContext());
        adapter.setItems(buildingsFilter);
        layoutManager = new GridLayoutManager(getContext(), 1);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setVisibility(View.VISIBLE);
        buildingsFilter.clear();
        buildingsFilter.addAll(buildings);

        return rootView;
    }

    private void filterState() {
        State state = (State) spinnerState.getSelectedItem();
        if (state.getId() != 0) {
         for (int i = buildingsFilter.size() - 1 ; i >= 0;i--){
                if (!buildingsFilter.get(i).getStateId().equals(state.getId() + ""))
                    buildingsFilter.remove(buildingsFilter.get(i));
            }
        }
        filterStatus();
    }

    private void filterStatus() {
        int p = spinnerStatus.getSelectedItemPosition();
        if (p != 0) {
                for (int i = buildingsFilter.size() - 1 ; i >= 0;i--){
                    if (!buildingsFilter.get(i).getType().equals(p + ""))
                        buildingsFilter.remove(buildingsFilter.get(i));
                }
        }
        filterTypes();
    }

    private void filterTypes() {
        int p = spinnerType.getSelectedItemPosition();
        if (p != 0) {
            for (int i = buildingsFilter.size() - 1 ; i >= 0;i--){
                if (!buildingsFilter.get(i).getAdsType().equals(p + ""))
                    buildingsFilter.remove(buildingsFilter.get(i));
            }
        }
        display();
    }

    private void display() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MainActivityCallback) {
            callback = (MainActivityCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;

    }
}
