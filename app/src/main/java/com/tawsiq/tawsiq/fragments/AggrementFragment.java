package com.tawsiq.tawsiq.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.tawsiq.tawsiq.Models.Package;
import com.tawsiq.tawsiq.R;
import com.tawsiq.tawsiq.interfaces.SignupActivityCallback;
import com.tawsiq.tawsiq.utils.utils;

import java.util.ArrayList;
import java.util.List;

public class AggrementFragment extends Fragment {


    private SignupActivityCallback mListener;
    private View progress;
    private TextView text;
    private CheckBox accept;
    private Button signup;

    public AggrementFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_aggrement, container, false);
        progress = view.findViewById(R.id.progress_bar);
        accept = view.findViewById(R.id.accept);
        signup = view.findViewById(R.id.signup);
        accept.setEnabled(false);
        signup.setEnabled(false);
        text = view.findViewById(R.id.text);
        load();
        accept.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    signup.setEnabled(true);
                else
                    signup.setEnabled(false);
            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.nextStep();
            }
        });
        return view;
    }

    private void load() {
        Ion.with(this)
                .load("https://tawthiq.com.sa/api/agremments")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        progress.setVisibility(View.GONE);
                        accept.setEnabled(true);
                        text.setText(utils.getAR(result.get("agremments").getAsString()));
                    }
                });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SignupActivityCallback) {
            mListener = (SignupActivityCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


}
