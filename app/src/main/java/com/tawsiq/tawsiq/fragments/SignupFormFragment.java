package com.tawsiq.tawsiq.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.tawsiq.tawsiq.Models.State;
import com.tawsiq.tawsiq.R;
import com.tawsiq.tawsiq.interfaces.SignupActivityCallback;
import com.tawsiq.tawsiq.utils.Preferences;

import java.util.List;

public class SignupFormFragment extends Fragment {

        private SignupActivityCallback mListener;
    EditText nameET, passwordET, passwordConfET, addrssET, emailET, phoneET;
    Spinner spinner;
    private ArrayAdapter<State> arrayAdapter;
    private Preferences preferances;
    List<State> states;
    public SignupFormFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_signup_form, container, false);

        nameET = view.findViewById(R.id.name);
        passwordET = view.findViewById(R.id.password);
        passwordConfET = view.findViewById(R.id.password_conf);
        addrssET = view.findViewById(R.id.address);
        phoneET = view.findViewById(R.id.phone);
        emailET = view.findViewById(R.id.email);
        spinner = view.findViewById(R.id.State);

        preferances = new Preferences(getContext());
        states = preferances.getAllStates();

        arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1,states);
        spinner.setAdapter(arrayAdapter);

        view.findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean error = false;

                if (nameET.getText().toString().isEmpty()) {
                    nameET.setError("مطلوب");
                    error = true;
                } if (passwordET.getText().toString().isEmpty()) {
                    passwordET.setError("مطلوب");
                    error = true;
                }
                if (emailET.getText().toString().isEmpty()) {
                    emailET.setError("مطلوب");
                    error = true;
                }
                if (passwordConfET.getText().toString().isEmpty()) {
                    passwordConfET.setError("مطلوب");
                    error = true;
                }
                if (addrssET.getText().toString().isEmpty()) {
                    addrssET.setError("مطلوب");
                    error = true;
                }
                if (phoneET.getText().toString().isEmpty()) {
                    phoneET.setError("مطلوب");
                    error = true;
                }
                if (emailET.getText().toString().isEmpty()) {
                    emailET.setError("مطلوب");
                    error = true;
                }
                if (!error) {
                    next();
                }
            }
        });
        return view;
    }

    private void next() {
        boolean error = false;

        if (!passwordET.getText().toString().equals(passwordConfET.getText().toString())) {
            passwordConfET.setError("كلمتان السر غير متطابقان");
            passwordET.setError("كلمتان السر غير متطابقان");
            error = true;
        } else {
            if (passwordET.getText().toString().length() < 6) {
                passwordConfET.setError("كلمة السر اقل من 6 حروف");
                passwordET.setError("كلمة السر اقل من 6 حروف");
                error = true;
            }
        }
        if ((!emailET.getText().toString().contains("@"))||(!emailET.getText().toString().contains("."))){
            emailET.setError("خطأ فى صيغة الايميل");
            error = true;
        }
        if (!error) {
            nextStep();
        }
    }

    private void nextStep() {
        mListener.nextStep(
                nameET.getText().toString(),
                emailET.getText().toString(),
                passwordET.getText().toString(),
                phoneET.getText().toString(),
                addrssET.getText().toString(),
                ((State)spinner.getSelectedItem()).getId()
        );

        System.out.println(((State)spinner.getSelectedItem()).getId());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SignupActivityCallback) {
            mListener = (SignupActivityCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
