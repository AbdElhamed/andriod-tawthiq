package com.tawsiq.tawsiq.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.tawsiq.tawsiq.Models.Package;
import com.tawsiq.tawsiq.R;
import com.tawsiq.tawsiq.interfaces.SignupActivityCallback;

import java.util.ArrayList;
import java.util.List;

public class BackagesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    private SignupActivityCallback mListener;
    private ViewPager pager;
    private TabLayout tabLayout;
    private SectionsPagerAdapter adapter;
    private View progress;
    private View next;

    public BackagesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_backages, container, false);
        pager = (ViewPager) view.findViewById(R.id.photos_viewpager);
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        progress =  view.findViewById(R.id.progress_bar);
        loadPackages();
        next = view.findViewById(R.id.next);
        next.setEnabled(false);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.nextStep(adapter.getPackage(pager.getCurrentItem()).getId());
                System.out.println();
            }
        });
        return view;
    }

    private void loadPackages() {
        Ion.with(this)
                .load("https://tawthiq.com.sa/api/backages")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        next.setEnabled(true);
                        progress.setVisibility(View.GONE);
                        JsonArray array = result.get("backages").getAsJsonArray();
                        Gson gson = new Gson();
                        ArrayList<Package> list = gson.fromJson(array, new TypeToken<List<Package>>() {
                        }.getType());
                        adapter = new SectionsPagerAdapter(getChildFragmentManager(), list);
                        pager.setAdapter(adapter);
                        tabLayout.setupWithViewPager(pager, true);
                    }
                });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SignupActivityCallback) {
            mListener = (SignupActivityCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        ArrayList<Package> packages;

        public SectionsPagerAdapter(FragmentManager fm, ArrayList<Package> packages) {
            super(fm);
            this.packages = packages;
        }

        @Override
        public Fragment getItem(int position) {
            return BackageFragment.newInstance(getActivity(),packages.get(position));
//            return BackageFragment.newInstance(packages.get(position).getRatio(),packages.get(position).getServicesLines(),packages.get(position).getServices());
        }
        private Package getPackage(int positon){
            return packages.get(positon);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return packages.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }

}
