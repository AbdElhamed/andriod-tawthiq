package com.tawsiq.tawsiq.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tawsiq.tawsiq.Models.Package;
import com.tawsiq.tawsiq.R;
import com.tawsiq.tawsiq.adapters.PackageServicesAdapter;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class BackageFragment extends Fragment {

    private Context context;
    private Package param;
    public BackageFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public BackageFragment(Context context,Package param) {
        this.context = context;
        this.param = param;
    }

    public static BackageFragment newInstance(Context context,Package param) {
        return new BackageFragment(context,param);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_backage, container, false);
//        ((TextView)view.findViewById(R.id.price)).setText(mParam1);
//        ((TextView)view.findViewById(R.id.text)).setText(mParam2);

        ((TextView)view.findViewById(R.id.price)).setText(param.getRatio());
        //((TextView)view.findViewById(R.id.text)).setText(param.getServicesLines());

        Log.e("SERVICES",param.getServices() + "");
        Log.e("SERVICES",param.getServices() + "");

        List<String> services = new ArrayList<>();

        try {
            JSONArray sers = new JSONArray(param.getServices());
            for (int i = 0; i < sers.length(); i++) {
                services.add(sers.getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RecyclerView packageServicesRecyclerView = view.findViewById(R.id.packageServicesRecyclerView);

        PackageServicesAdapter mAdapter = new PackageServicesAdapter(services);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context.getApplicationContext());
        packageServicesRecyclerView.setLayoutManager(mLayoutManager);

        packageServicesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        packageServicesRecyclerView.setAdapter(mAdapter);

        return view;
    }

}
