package com.tawsiq.tawsiq.interfaces;

import com.tawsiq.tawsiq.Models.Building;

import java.util.ArrayList;

public interface MainActivityCallback {
    ArrayList<Building> getBuildings();
}
