package com.tawsiq.tawsiq.interfaces;

public interface BuildingAdapterCallback {
    void updateView();
}
