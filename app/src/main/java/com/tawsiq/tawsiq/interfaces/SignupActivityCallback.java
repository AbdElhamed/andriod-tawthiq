package com.tawsiq.tawsiq.interfaces;

public interface SignupActivityCallback {
    void nextStep(
            String name,
            String email,
            String password,
            String phone,
            String address,
            int stateID
    );
    void nextStep(
            int packageID
    );
    void nextStep(
          );

}
