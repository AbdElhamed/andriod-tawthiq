package com.tawsiq.tawsiq.Models;

public class SubService {

    private String id;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String post_id;

    public String getPostId() {
        return this.post_id;
    }

    public void setPostId(String post_id) {
        this.post_id = post_id;
    }

    private String sub_title;

    public String getSubTitle() {
        return this.sub_title;
    }

    public void setSubTitle(String sub_title) {
        this.sub_title = sub_title;
    }

    private String feature;

    public String getFeature() {
        return this.feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    private String created_at;

    public String getCreatedAt() {
        return this.created_at;
    }

    public void setCreatedAt(String created_at) {
        this.created_at = created_at;
    }

    private String updated_at;

    public String getUpdatedAt() {
        return this.updated_at;
    }

    public void setUpdatedAt(String updated_at) {
        this.updated_at = updated_at;
    }

}
