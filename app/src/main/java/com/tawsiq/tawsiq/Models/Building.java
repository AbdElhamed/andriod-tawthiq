package com.tawsiq.tawsiq.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class Building implements Parcelable {
    public static final Creator<Building> CREATOR = new Creator<Building>() {
        @Override
        public Building createFromParcel(Parcel in) {
            return new Building(in);
        }

        @Override
        public Building[] newArray(int size) {
            return new Building[size];
        }
    };

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Building && this.getId() == ((Building) obj).getId();
    }

    private int id;
    private String user_id;
    private String branch_id;
    private String state_id;
    private String address_id;
    private String photo;
    private String name;
    private String address;
    private String ads_type;
    private String lng;
    private String lat;
    private String price;
    private String bathrooms;
    private String garages;
    private String description;
    private String date;
    private String special;
    private String type;
    private String purpose;
    private String approvement_type;
    private String area;
    private String engineering_drawings;
    private String video;
    private String created_at;
    private String updated_at;

    protected Building(Parcel in) {
        id = in.readInt();
        user_id = in.readString();
        branch_id = in.readString();
        state_id = in.readString();
        address_id = in.readString();
        photo = in.readString();
        name = in.readString();
        address = in.readString();
        ads_type = in.readString();
        lng = in.readString();
        lat = in.readString();
        price = in.readString();
        bathrooms = in.readString();
        garages = in.readString();
        description = in.readString();
        date = in.readString();
        special = in.readString();
        type = in.readString();
        purpose = in.readString();
        approvement_type = in.readString();
        area = in.readString();
        engineering_drawings = in.readString();
        video = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return this.user_id;
    }

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    public String getBranchId() {
        return this.branch_id;
    }

    public void setBranchId(String branch_id) {
        this.branch_id = branch_id;
    }

    public String getStateId() {
        return this.state_id;
    }

    public void setStateId(String state_id) {
        this.state_id = state_id;
    }

    public String getAddressId() {
        return this.address_id;
    }

    public void setAddressId(String address_id) {
        this.address_id = address_id;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAdsType() {
        return this.ads_type;
    }

    public void setAdsType(String ads_type) {
        this.ads_type = ads_type;
    }

    public String getLng() {
        return this.lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return this.lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getBathrooms() {
        return this.bathrooms;
    }

    public void setBathrooms(String bathrooms) {
        this.bathrooms = bathrooms;
    }

    public String getGarages() {
        return this.garages;
    }

    public void setGarages(String garages) {
        this.garages = garages;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSpecial() {
        return this.special;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPurpose() {
        return this.purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getApprovementType() {
        return this.approvement_type;
    }

    public void setApprovementType(String approvement_type) {
        this.approvement_type = approvement_type;
    }

    public String getArea() {
        return this.area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getEngineeringDrawings() {
        return this.engineering_drawings;
    }

    public void setEngineeringDrawings(String engineering_drawings) {
        this.engineering_drawings = engineering_drawings;
    }

    public String getVideo() {
        return this.video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getCreatedAt() {
        return this.created_at;
    }

    public void setCreatedAt(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdatedAt() {
        return this.updated_at;
    }

    public void setUpdatedAt(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(user_id);
        dest.writeString(branch_id);
        dest.writeString(state_id);
        dest.writeString(address_id);
        dest.writeString(photo);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(ads_type);
        dest.writeString(lng);
        dest.writeString(lat);
        dest.writeString(price);
        dest.writeString(bathrooms);
        dest.writeString(garages);
        dest.writeString(description);
        dest.writeString(date);
        dest.writeString(special);
        dest.writeString(type);
        dest.writeString(purpose);
        dest.writeString(approvement_type);
        dest.writeString(area);
        dest.writeString(engineering_drawings);
        dest.writeString(video);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }
}