package com.tawsiq.tawsiq.Models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class Package implements Parcelable {

    public static final Creator<Package> CREATOR = new Creator<Package>() {
        @NonNull
        @Override
        public Package createFromParcel(Parcel in) {
            return new Package(in);
        }

        @Override
        public Package[] newArray(int size) {
            return new Package[size];
        }
    };
    private int id;
    private String ratio;
    private String services;
    private String active;
    private String created_at;
    private String updated_at;

    protected Package(Parcel in) {
        id = in.readInt();
        ratio = in.readString();
        services = in.readString();
        active = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(ratio);
        dest.writeString(services);
        dest.writeString(active);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRatio() {
        return this.ratio;
    }

    public void setRatio(String ratio) {
        this.ratio = ratio;
    }

    public String getServices() {
        return this.services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getActive() {
        return this.active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCreatedAt() {
        return this.created_at;
    }

    public void setCreatedAt(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdatedAt() {
        return this.updated_at;
    }

    public void setUpdatedAt(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getServicesLines(){
        List<String> jsonArray = new Gson().fromJson(services, new TypeToken<List<String>>() {
        }.getType());
        StringBuilder s = new StringBuilder();
        for (String x :jsonArray
             ) {
            s.append(x);
            s.append("\n");
        }
        return s.toString();
    }

}
