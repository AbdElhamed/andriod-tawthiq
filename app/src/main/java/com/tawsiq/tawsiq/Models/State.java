package com.tawsiq.tawsiq.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class State implements Parcelable {
    public static final Creator<State> CREATOR = new Creator<State>() {
        @Override
        public State createFromParcel(Parcel in) {
            return new State(in);
        }

        @Override
        public State[] newArray(int size) {
            return new State[size];
        }
    };
    private int id;
    private String state_name;
    private String disapled;
    private String created_at;
    private String updated_at;

    public State() {
    }

    protected State(Parcel in) {
        id = in.readInt();
        state_name = in.readString();
        disapled = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    @Override
    public String toString() {
        return
                state_name;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStateName() {
        return this.state_name;
    }

    public void setStateName(String state_name) {
        this.state_name = state_name;
    }

    public String getDisapled() {
        return this.disapled;
    }

    public void setDisapled(String disapled) {
        this.disapled = disapled;
    }

    public String getCreatedAt() {
        return this.created_at;
    }

    public void setCreatedAt(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdatedAt() {
        return this.updated_at;
    }

    public void setUpdatedAt(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(state_name);
        dest.writeString(disapled);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }
}
