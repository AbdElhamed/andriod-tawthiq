package com.tawsiq.tawsiq.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
    private int id;
    private String role;
    private String user_name;
    private String email;
    private String full_name;
    private String photo;
    private String phone;
    private String address_id;
    private String active;
    private String created_at;
    private String updated_at;

    protected User(Parcel in) {
        id = in.readInt();
        role = in.readString();
        user_name = in.readString();
        email = in.readString();
        full_name = in.readString();
        photo = in.readString();
        phone = in.readString();
        address_id = in.readString();
        active = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", role='" + role + '\'' +
                ", user_name='" + user_name + '\'' +
                ", email='" + email + '\'' +
                ", full_name='" + full_name + '\'' +
                ", photo='" + photo + '\'' +
                ", phone='" + phone + '\'' +
                ", address_id='" + address_id + '\'' +
                ", active='" + active + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                '}';
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUserName() {
        return this.user_name;
    }

    public void setUserName(String user_name) {
        this.user_name = user_name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return this.full_name;
    }

    public void setFullName(String full_name) {
        this.full_name = full_name;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddressId() {
        return this.address_id;
    }

    public void setAddressId(String address_id) {
        this.address_id = address_id;
    }

    public String getActive() {
        return this.active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCreatedAt() {
        return this.created_at;
    }

    public void setCreatedAt(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdatedAt() {
        return this.updated_at;
    }

    public void setUpdatedAt(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(role);
        dest.writeString(user_name);
        dest.writeString(email);
        dest.writeString(full_name);
        dest.writeString(photo);
        dest.writeString(phone);
        dest.writeString(address_id);
        dest.writeString(active);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }
}