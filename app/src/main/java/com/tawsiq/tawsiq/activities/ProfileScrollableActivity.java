package com.tawsiq.tawsiq.activities;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.tawsiq.tawsiq.Models.Building;
import com.tawsiq.tawsiq.R;
import com.tawsiq.tawsiq.adapters.BuildingsAdapter;
import com.tawsiq.tawsiq.utils.Preferences;

import java.util.ArrayList;

public class ProfileScrollableActivity extends AppCompatActivity {

    private CollapsingToolbarLayout collapsingToolbarLayout = null;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private BuildingsAdapter adapter;
    private ArrayList<Building> buildings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_scrollable);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle("");

        dynamicToolbarColor();
        toolbarTextAppearance();

        Preferences pre = new Preferences(this);

        ((TextView) findViewById(R.id.name)).setText(pre.getUser().getFullName());
        ((TextView) findViewById(R.id.phone)).setText(pre.getUser().getPhone());
        ((TextView) findViewById(R.id.email)).setText(pre.getUser().getEmail());

        TextView state = findViewById(R.id.state);

        if(pre.getState(Integer.parseInt(pre.getUser().getAddressId())) != null){
            state.setText(pre.getState(Integer.parseInt(pre.getUser().getAddressId())).toString());
        }else {
            state.setVisibility(View.GONE);
        }

        recyclerView = findViewById(R.id.recycler_view);
        adapter = new BuildingsAdapter(this);
        buildings = new ArrayList<>();
        adapter.setItems(buildings);
        layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setVisibility(View.VISIBLE);
        ArrayList<Building> buildingArrayList = getIntent().getParcelableArrayListExtra("Buildings");
        for (Building b : buildingArrayList) {
            if(b.getUserId().equals(pre.getUser().getId()+""))
                buildings.add(b);
        }
        adapter.notifyDataSetChanged();
        if(buildings.size()<1){
            findViewById(R.id.empty).setVisibility(View.VISIBLE);
        }
    }

    private void dynamicToolbarColor() {

//        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
//                R.drawable.ic_person_black_24dp);
//        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
//
//            @Override
//            public void onGenerated(@NonNull Palette palette) {
//                //collapsingToolbarLayout.setContentScrimColor(palette.getMutedColor(R.attr.colorPrimary));
//                //collapsingToolbarLayout.setStatusBarScrimColor(palette.getMutedColor(R.attr.colorPrimaryDark));
//            }
//        });
    }

    private void toolbarTextAppearance() {
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
    }
}