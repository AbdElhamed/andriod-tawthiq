package com.tawsiq.tawsiq.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.tawsiq.tawsiq.Models.Building;
import com.tawsiq.tawsiq.R;
import com.tawsiq.tawsiq.utils.GifImageView;
import com.tawsiq.tawsiq.utils.Preferences;

import java.util.ArrayList;
import java.util.List;

public class SplashActivity extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 1000;
    private Preferences preferances;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ((GifImageView)findViewById(R.id.loading)).setGifImageResource(R.drawable.logo);
        loadStates();
    }

    private void getBuildings() {
        Ion.with(this)
                .load("https://tawthiq.com.sa/api/building/")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        Log.e("BUILDINGS", "" + result.toString());

                        JsonArray array = result.get("buildings").getAsJsonArray();
                        Gson gson = new Gson();
                        ArrayList<Building> list = gson.fromJson(array, new TypeToken<List<Building>>() {
                        }.getType());
                        System.out.println(list.size() + "    " +list.get(0).getName());
                        Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                        mainIntent.putExtra(MainActivity.EXTRA_BUILDINGS, list);
                        SplashActivity.this.startActivity(mainIntent);
                        SplashActivity.this.finish();
                    }
                });
    }

    private void loadStates() {
        Ion.with(this)
                .load("https://tawthiq.com.sa/api/states")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if(result != null) {
                            JsonArray array = result.get("allstates").getAsJsonArray();
                            preferances = new Preferences(SplashActivity.this);
                            preferances.setStates(array.toString());
                            getBuildings();
                            System.out.println(preferances.getAllStates());
                        }else {
                            loadStates();
                        }
                    }
                });
    }
}
