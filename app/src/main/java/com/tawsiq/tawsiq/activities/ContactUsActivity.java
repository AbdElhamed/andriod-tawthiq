package com.tawsiq.tawsiq.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.tawsiq.tawsiq.R;

public class ContactUsActivity extends AppCompatActivity {
    EditText message, email, name;
    Button gamosa;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        progressBar = findViewById(R.id.progress);
        gamosa = findViewById(R.id.gamos);
        message = findViewById(R.id.message);
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);

        gamosa.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean error = false;
                        if (email.getText().toString().isEmpty()) {
                            email.setError("مطلوب");
                            error = true;
                        }
                        if (name.getText().toString().isEmpty()) {
                            name.setError("مطلوب");
                            error = true;
                        }
                        if (message.getText().toString().isEmpty()) {
                            message.setError("مطلوب");
                            error = true;
                        }
                        if (!error) {
                            send();
                        }
                    }
                });
    }

    private void send() {
        progressBar.setVisibility(View.VISIBLE);
        gamosa.setEnabled(false);
        JsonObject prams = new JsonObject();
        prams.addProperty("email", email.getText().toString());
        prams.addProperty("name", name.getText().toString());
        prams.addProperty("message", message.getText().toString());

        Ion.with(this)
                .load("https://tawthiq.com.sa/api/contactus")
//                .addQuery("email", email.getText().toString())
//                .addQuery("name", name.getText().toString())
//                .addQuery("message", message.getText().toString())
                .setJsonObjectBody(prams)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        gamosa.setEnabled(true);
                        progressBar.setVisibility(View.GONE);
                        if (result != null) {
                            System.out.println(result);
                            if (result.get("status").getAsBoolean()) {
                                Toast.makeText(ContactUsActivity.this, "تم تسجيل الرسالة بنجاح", Toast.LENGTH_SHORT).show();
                                finish();
                            }

                        } else

                        {
                            System.out.println(e.getMessage());
                            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Connection error !", Snackbar.LENGTH_INDEFINITE)
                                    .setAction("Retry", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            send();
                                        }
                                    });
                            snackbar.show();
                        }
                    }
                });


    }

}
