package com.tawsiq.tawsiq.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.tawsiq.tawsiq.Models.Building;
import com.tawsiq.tawsiq.R;
import com.tawsiq.tawsiq.adapters.BuildingsAdapter;
import com.tawsiq.tawsiq.interfaces.BuildingAdapterCallback;
import com.tawsiq.tawsiq.interfaces.MainActivityCallback;
import com.tawsiq.tawsiq.utils.Preferences;

import java.util.ArrayList;
import java.util.List;

public class FavoritesActivity extends AppCompatActivity implements BuildingAdapterCallback {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private BuildingsAdapter adapter;
    private ArrayList<Building> buildings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("المفضلة");


        recyclerView = findViewById(R.id.recycler_view);
        adapter = new BuildingsAdapter(this);
        buildings = new ArrayList<>();
        adapter.setItems(buildings);
        layoutManager = new GridLayoutManager(this, 1);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setVisibility(View.VISIBLE);


    }


    @Override
    public void updateView() {
        buildings.clear();
        adapter.notifyDataSetChanged();
        Preferences preferences = new Preferences(this);
        List<Building> favorites = preferences.getAllFavorites();
        if (favorites.size() > 0) {
            buildings.addAll(favorites);
            adapter.notifyDataSetChanged();
        } else {
            Snackbar snackbar =Snackbar.make(findViewById(android.R.id.content), "لم يتم اضافة عقارات بعد", Snackbar.LENGTH_INDEFINITE)
                    .setAction("رجوع", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
            snackbar.getView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            snackbar.getView().setTextDirection(View.TEXT_DIRECTION_RTL);
            snackbar.show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        updateView();
    }
}
