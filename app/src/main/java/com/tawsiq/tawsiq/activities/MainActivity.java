package com.tawsiq.tawsiq.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.tawsiq.tawsiq.Models.Building;
import com.tawsiq.tawsiq.R;
import com.tawsiq.tawsiq.fragments.HomeFragment;
import com.tawsiq.tawsiq.fragments.MapFragment;
import com.tawsiq.tawsiq.fragments.SearchFragment;
import com.tawsiq.tawsiq.interfaces.MainActivityCallback;
import com.tawsiq.tawsiq.utils.Preferences;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MainActivityCallback {
    public static final String EXTRA_BUILDINGS = "buildingsList";
    private ArrayList<Building> buildings;
    private NavigationView navigationView;
    private Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buildings = getIntent().getParcelableArrayListExtra(EXTRA_BUILDINGS);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        preferences = new Preferences(this);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_home);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeFragment()).commit();
        getSupportActionBar().setTitle(navigationView.getMenu().findItem(R.id.nav_home).getTitle());

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        hideItem();
    }

    private void hideItem() {
        Menu nav_Menu = navigationView.getMenu();
        if (preferences.getUser() == null) {
            nav_Menu.findItem(R.id.nav_login).setVisible(true);
            nav_Menu.findItem(R.id.nav_profile).setVisible(false);
            nav_Menu.findItem(R.id.nav_logout).setVisible(false);
        }else {
            nav_Menu.findItem(R.id.nav_login).setVisible(false);
            nav_Menu.findItem(R.id.nav_profile).setVisible(true);
            nav_Menu.findItem(R.id.nav_logout).setVisible(true);

        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new HomeFragment()).commit();
        } else if (id == R.id.nav_aqarat) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new MapFragment()).commit();
        } else if (id == R.id.nav_search) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new SearchFragment()).commit();

        } else if (id == R.id.nav_profile) {
            Intent i = new Intent(MainActivity.this, ProfileScrollableActivity.class);
            //Intent i = new Intent(MainActivity.this, ProfileActivity.class);
            i.putExtra("Buildings", getBuildings());
            startActivity(i);
        } else if (id == R.id.nav_favorites) {
            startActivity(new Intent(MainActivity.this, FavoritesActivity.class));
        } else if (id == R.id.nav_services) {
            startActivity(new Intent(MainActivity.this, ServicesActivity.class));
        } else if (id == R.id.nav_contact) {
            startActivity(new Intent(MainActivity.this, ContactUsActivity.class));
        } else if (id == R.id.nav_login) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
        } else if (id == R.id.nav_logout) {
            preferences.clearUser();
            hideItem();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        if (item.isCheckable())
            getSupportActionBar().setTitle(item.getTitle());
        return true;
    }

    @Override
    public ArrayList<Building> getBuildings() {
        return buildings;
    }
}
