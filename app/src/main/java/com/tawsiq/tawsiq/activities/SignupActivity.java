package com.tawsiq.tawsiq.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.tawsiq.tawsiq.R;
import com.tawsiq.tawsiq.fragments.AggrementFragment;
import com.tawsiq.tawsiq.fragments.BackagesFragment;
import com.tawsiq.tawsiq.fragments.SignupFormFragment;
import com.tawsiq.tawsiq.interfaces.SignupActivityCallback;

public class SignupActivity extends AppCompatActivity implements SignupActivityCallback {

    String name;
    String email;
    String password;
    String phone;
    String address;
    int stateID;
    int packgageID;
    private View progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        progressBar = findViewById(R.id.progress);
        progressBar.setVisibility(View.GONE);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new SignupFormFragment()).commit();
    }

    @Override
    public void nextStep(String name, String email, String password, String phone, String address, int stateID) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.phone = phone;
        this.address = address;
        this.stateID = stateID;
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new BackagesFragment()).commit();

    }

    @Override
    public void nextStep(int packageID) {
        this.packgageID = packageID;
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new AggrementFragment()).commit();
    }

    @Override
    public void nextStep() {
        signUp();
    }

    private void signUp() {
        System.out.println(name);
        System.out.println(email);
        System.out.println(phone);
        System.out.println(password);
        System.out.println(address);
        System.out.println(stateID);
        System.out.println(packgageID);
        progressBar.setVisibility(View.VISIBLE);

        JsonObject body = new JsonObject();
        body.addProperty("email", email);
        body.addProperty("password", password);
        body.addProperty("phone", phone);
        body.addProperty("address_description", address);
        body.addProperty("full_name", name);
        body.addProperty("address_state", stateID + "");
        body.addProperty("package_id", packgageID + "");
        body.addProperty("agreement", "1");

        Ion.with(this)
                .load("POST","https://tawthiq.com.sa/api/register")
                .addHeader("Content-Type", "application/json")
//                .addQuery("email", email)
//                .addQuery("password", password)
//                .addQuery("phone", phone)
//                .addQuery("address_description", address)
//                .addQuery("full_name", name)
//                .addQuery("address_state", stateID + "")
//                .addQuery("package_id", packgageID + "")
//                .addQuery("agreement", "1")
                .setJsonObjectBody(body)

//                .asString()
//                .setCallback(new FutureCallback<String>() {
//                    @Override
//                    public void onCompleted(Exception e, String result) {
//                        progressBar.setVisibility(View.GONE);
//                        if (result != null) {
//                            System.out.println(result);
//
//                        } else {
//                            e.printStackTrace();
//                            System.out.println(e.getMessage());
//                            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Connection error !", Snackbar.LENGTH_INDEFINITE)
//                                    .setAction("Retry", new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View v) {
//                                            signUp();
//                                        }
//                                    });
//                            snackbar.show();
//                        }
//                    }
//                });

                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        progressBar.setVisibility(View.GONE);
                        if (result != null) {
                            System.out.println(result);
                            if (result.get("status").getAsBoolean()) {
                                Toast.makeText(SignupActivity.this,"تم تسجيل الحساب بنجاح سيتم تأكيده فى اقرب وقت.",Toast.LENGTH_LONG).show();
                                finish();
                            }
                        } else {
                            e.printStackTrace();
                            System.out.println(e.getMessage());
                            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Connection error !", Snackbar.LENGTH_INDEFINITE)
                                    .setAction("Retry", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            signUp();
                                        }
                                    });
                            snackbar.show();
                        }
                    }
                });
    }

    private void retry() {
        signUp();
    }
}

