package com.tawsiq.tawsiq.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.tawsiq.tawsiq.Models.Building;
import com.tawsiq.tawsiq.R;
import com.tawsiq.tawsiq.adapters.BuildingsAdapter;
import com.tawsiq.tawsiq.utils.Preferences;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private BuildingsAdapter adapter;
    private ArrayList<Building> buildings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();

        Preferences pre = new Preferences(this);

        ((TextView) findViewById(R.id.name)).setText(pre.getUser().getFullName());
        ((TextView) findViewById(R.id.phone)).setText(pre.getUser().getPhone());
        ((TextView) findViewById(R.id.email)).setText(pre.getUser().getEmail());
        ((TextView) findViewById(R.id.state)).setText(pre.getState(Integer.parseInt(pre.getUser().getAddressId())) != null ?
                pre.getState(Integer.parseInt(pre.getUser().getAddressId())).toString() : "");

        recyclerView = findViewById(R.id.recycler_view);
        adapter = new BuildingsAdapter(this);
        buildings = new ArrayList<>();
        adapter.setItems(buildings);
        layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setVisibility(View.VISIBLE);
        ArrayList<Building> buildingArrayList = getIntent().getParcelableArrayListExtra("Buildings");
        for (Building b : buildingArrayList) {
                if(b.getUserId().equals(pre.getUser().getId()+""))
                    buildings.add(b);
        }
        adapter.notifyDataSetChanged();
        if(buildings.size()<1){
            findViewById(R.id.empty).setVisibility(View.VISIBLE);
        }
    }
}
