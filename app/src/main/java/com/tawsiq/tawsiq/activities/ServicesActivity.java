package com.tawsiq.tawsiq.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.tawsiq.tawsiq.Models.Service;
import com.tawsiq.tawsiq.Models.SubService;
import com.tawsiq.tawsiq.R;
import com.tawsiq.tawsiq.utils.utils;

import java.util.ArrayList;
import java.util.List;

public class ServicesActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private ArrayList<Service> services;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("الخدمات");
        getServices();
        services=new ArrayList<>();
        mViewPager = (ViewPager) findViewById(R.id.container);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        tabLayout.setupWithViewPager(mViewPager);
    }


    private void getServices() {
        Ion.with(this)
                .load("https://tawthiq.com.sa/api/services")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        JsonArray array = result.get("allposts").getAsJsonArray();
                        JsonArray arrayw2 = result.get("more_services").getAsJsonArray();
                        Gson gson = new Gson();
                        ArrayList<Service> services = gson.fromJson(array, new TypeToken<List<Service>>() {
                        }.getType());
                        ArrayList<SubService> subServices = gson.fromJson(arrayw2, new TypeToken<List<SubService>>() {
                        }.getType());
                        for (int i = 0 ; i < services.size() ; i++){
                            List<SubService> subServ = new ArrayList<>();
                            for (SubService su : subServices) {

                                if (services.get(i).getId().equals(su.getPostId())) {
                                    subServ.add(su);
                                }
                            }
                            services.get(1).setSubServices(subServ);
                        }

                        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), services);
                        mViewPager.setAdapter(mSectionsPagerAdapter);

                        mViewPager.setCurrentItem(mSectionsPagerAdapter.getCount()-1);
                    }
                });


    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */

        LinearLayout layout;
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(Service service) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putParcelable(ARG_SECTION_NUMBER, service);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            Service service = getArguments().getParcelable(ARG_SECTION_NUMBER);
            View rootView = inflater.inflate(R.layout.fragment_service, container, false);
            layout = rootView.findViewById(R.id.sub_servecis);
            ((TextView) rootView.findViewById(R.id.body)).setText(utils.getAR(service.getBody()));
            List<String> strings = utils.getStringList(utils.getAR(service.getFeature()));
            for (String s : strings) {
                TextView tv = (TextView) inflater.inflate(R.layout.item_feature, null);
                tv.setText(s);
                ((LinearLayout) rootView.findViewById(R.id.features)).addView(tv);

            }
            if(service.getSubServices() !=null)




            for (SubService s : service.getSubServices()) {
                System.out.println("++"+utils.getAR(s.getSubTitle()));
                View servView = inflater.inflate(R.layout.service_item, null);
                ((TextView) servView.findViewById(R.id.body)).setText(utils.getAR(s.getSubTitle()));
                List<String> strings2 = utils.getStringList(utils.getAR(s.getFeature()));
                for (String s2 : strings2) {
                    System.out.println("xx"+s2);
                    TextView tv2 = (TextView) inflater.inflate(R.layout.item_feature, null);
                    tv2.setText(s2);
                    ((LinearLayout) servView.findViewById(R.id.features)).addView(tv2);

                }

                layout.addView(servView);

            }
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        ArrayList<Service> services;


        public SectionsPagerAdapter(FragmentManager fm, ArrayList<Service> Services) {
            super(fm);
            services = Services;
        }


        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(services.get(position));
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return utils.getAR(services.get(position).getTitle());
        }

        @Override
        public int getCount() {
            return services.size();
        }
    }
}
