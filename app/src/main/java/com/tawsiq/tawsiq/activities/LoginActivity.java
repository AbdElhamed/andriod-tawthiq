package com.tawsiq.tawsiq.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.tawsiq.tawsiq.R;
import com.tawsiq.tawsiq.utils.Preferences;

public class LoginActivity extends AppCompatActivity {
    EditText passwordET, emailET;
    Button loginBTN;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        emailET = findViewById(R.id.email);
        passwordET = findViewById(R.id.password);
        loginBTN = findViewById(R.id.login);
        progressBar = findViewById(R.id.progress);
        progressBar.setVisibility(View.GONE);
        findViewById(R.id.forget).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://tawthiq.com.sa/password/reset"));
                startActivity(browserIntent);
            }
        });
        findViewById(R.id.new_user).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, SignupActivity.class));
                finish();
            }
        });
        loginBTN.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean error = false;
                        if (passwordET.getText().toString().isEmpty()) {
                            passwordET.setError("مطلوب");
                            error = true;
                        }
                        if (emailET.getText().toString().isEmpty()) {
                            emailET.setError("مطلوب");
                            error = true;
                        }
                        if (!error) {
                            login();
                        }
                    }
                });

    }

    private void login() {
        progressBar.setVisibility(View.VISIBLE);
        loginBTN.setEnabled(false);
        Ion.with(this)
                .load("post","https://tawthiq.com.sa/api/login")
                .addQuery("email", emailET.getText().toString())
                .addQuery("password", passwordET.getText().toString())
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        loginBTN.setEnabled(true);
                        progressBar.setVisibility(View.GONE);

                        Log.e("RESULT","" + result.toString());

                        if (result != null) {
                            System.out.println(result);
                            if (result.get("status").getAsBoolean()) {
                                JsonObject userJson = result.get("user").getAsJsonObject();
                                Preferences preferences = new Preferences(LoginActivity.this);
                                preferences.setUser(userJson.toString());
                                Toast.makeText(LoginActivity.this, "تم تسجيل الدخول بنجاح", Toast.LENGTH_SHORT).show();
                                System.out.println(preferences.getUser());
                                finish();
                            } else {
                                String message = "تأكد من صيغة الايميل";
                                try {
                                    message = result.get("messages").getAsString();
                                } catch (UnsupportedOperationException error) {
                                    error.printStackTrace();
                                }
                                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_INDEFINITE);
                                snackbar.getView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                                snackbar.getView().setTextDirection(View.TEXT_DIRECTION_RTL);
                                snackbar.show();
                            }

                        } else {
                            e.printStackTrace();
                            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Connection error !", Snackbar.LENGTH_INDEFINITE)
                                    .setAction("Retry", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            login();
                                        }
                                    });
                            snackbar.show();
                        }
                    }
                });

    }

}
