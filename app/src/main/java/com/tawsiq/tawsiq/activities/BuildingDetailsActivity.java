package com.tawsiq.tawsiq.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.tawsiq.tawsiq.Models.Building;
import com.tawsiq.tawsiq.R;
import com.tawsiq.tawsiq.fragments.ImageFragment;
import com.tawsiq.tawsiq.utils.Preferences;
import com.tawsiq.tawsiq.utils.utils;

import java.util.List;

public class BuildingDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static final String EXTRA_BUILDING = "Building";
    private Building building;
    private Preferences preferences;
    private ImageView favorite;
    private TextView name, price, city, description;
    MapView mapView;
    GoogleMap mMap;
    private LinearLayout commentsLayout, custattLayout;
    private ImageView image;
    private EditText cName, cMail, cText;
    private Button loginBTN;
    private ProgressBar progressBar;
    private SectionsPagerAdapter adapter, adapter2;
    private ViewPager pager, pager2;
    private TabLayout tabLayout, tabLayout2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_building_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        building = getIntent().getParcelableExtra(EXTRA_BUILDING);
        preferences = new Preferences(this);

        mapView = findViewById(R.id.map_view);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        progressBar = findViewById(R.id.progress);
        cName = findViewById(R.id.comment_name);
        cText = findViewById(R.id.comment_text);
        cMail = findViewById(R.id.comment_mail);
        loginBTN = findViewById(R.id.comment_btn);
        loginBTN.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean error = false;
                        if (cMail.getText().toString().isEmpty()) {
                            cMail.setError("مطلوب");
                            error = true;
                        }
                        if (cText.getText().toString().isEmpty()) {
                            cText.setError("مطلوب");
                            error = true;
                        }
                        if (cName.getText().toString().isEmpty()) {
                            cName.setError("مطلوب");
                            error = true;
                        }
                        if (!error) {
                            addComment();
                        }
                    }
                });


        favorite = findViewById(R.id.favorite);
        image = findViewById(R.id.image);
        name = findViewById(R.id.name);
        price = findViewById(R.id.price);
        city = findViewById(R.id.city);
        description = findViewById(R.id.description);

        if (building.getVideo() == null) {
            findViewById(R.id.video_layout).setVisibility(View.GONE);
        } else {
            findViewById(R.id.video_layout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(building.getVideo()));
                    startActivity(browserIntent);
                }
            });
        }

        ((TextView) findViewById(R.id.area)).append(" " + building.getArea() + " متر مربع.");
        ((TextView) findViewById(R.id.path)).append(" " + building.getBathrooms() + " حمام.");
        ((TextView) findViewById(R.id.grag)).append(" " + building.getGarages() + " جراج.");

        pager = (ViewPager) findViewById(R.id.photos_viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        pager2 = (ViewPager) findViewById(R.id.photos_viewpager2);
        tabLayout2 = (TabLayout) findViewById(R.id.tab_layout2);

        adapter = new SectionsPagerAdapter(getSupportFragmentManager(), utils.getStringList(building.getPhoto()));
        pager.setAdapter(adapter);
        tabLayout.setupWithViewPager(pager, true);

        if (building.getEngineeringDrawings() != null && utils.getStringList(building.getEngineeringDrawings()).size() > 0) {
            adapter2 = new SectionsPagerAdapter(getSupportFragmentManager(), utils.getStringList(building.getEngineeringDrawings()));
            pager2.setAdapter(adapter2);
            tabLayout2.setupWithViewPager(pager2, true);
        } else {
            findViewById(R.id.engph_layout).setVisibility(View.GONE);
        }

        displayFavoritStatus();
        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (preferences.isFavorite(building)) {
                    preferences.removeFavorite(building);
                } else {
                    preferences.AddToFavorite(building);
                }
                displayFavoritStatus();
            }
        });

        commentsLayout = findViewById(R.id.comments_layout);
        custattLayout = findViewById(R.id.custatt_layout);
        name.setText(utils.getAR(building.getName()));
        description.setText(utils.getAR(building.getDescription()));
        price.setText(building.getPrice().concat(" ر.س."));
        city.setText(preferences.getState(Integer.parseInt(building.getStateId())).toString());

        loadRest();
    }

    private void displayCustom(List<String> strings) {
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (String s : strings) {
            TextView tv = (TextView) inflater.inflate(R.layout.item_custom_att, null);
            switch (s) {
                case "swimming-pool":
                    tv.setText(" حمام سباحة");
                    break;
                case "security":
                    tv.setText(" حراسة");
                    break;
                case "garden":
                    tv.setText(" حديقة");
                    break;
                case "garages":
                    tv.setText(" جراجات");
                    break;
                default:
                    tv.setText(s);
                    break;
            }
            custattLayout.addView(tv);
        }
    }

    private void loadRest() {
        Ion.with(this)
                .load("https://tawthiq.com.sa/api/singlebuilding/" + building.getId())
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        JsonObject array = result.get("buildings").getAsJsonObject();
                        displayCustom(utils.getStringList(array.getAsJsonObject("building_extra").get("value").getAsString()));
                        JsonArray building_comments = array.getAsJsonArray("building_comments");
                        for (int i = 0; i < building_comments.size(); i++) {
                            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + i);
                            JsonObject object = building_comments.get(i).getAsJsonObject();
                            LayoutInflater inflater = (LayoutInflater) BuildingDetailsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View tv = inflater.inflate(R.layout.item_comment, null);
                            ((TextView) tv.findViewById(R.id.name)).setText(object.get("commentator_name").getAsString());
                            ((TextView) tv.findViewById(R.id.text)).setText(object.get("body").getAsString());
                            commentsLayout.addView(tv);
                        }
                    }
                });
    }

    private void addComment() {
        progressBar.setVisibility(View.VISIBLE);
        loginBTN.setEnabled(false);
        JsonObject json = new JsonObject();
        json.addProperty("commentator_email", cMail.getText().toString());
        json.addProperty("commentator_name", cName.getText().toString());
        json.addProperty("body", cText.getText().toString());
        json.addProperty("model_id", building.getId() + "");
        json.addProperty("model_type", "buildings");

        Ion.with(this)
                .load("https://tawthiq.com.sa/api/comment")
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        loginBTN.setEnabled(true);
                        progressBar.setVisibility(View.GONE);
                        if (result != null) {
                            System.out.println(result);
                            Toast.makeText(BuildingDetailsActivity.this, "تم تسجيل التعليق بنجاح", Toast.LENGTH_SHORT).show();
                        } else

                        {
                            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Connection error !", Snackbar.LENGTH_INDEFINITE)
                                    .setAction("Retry", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            addComment();
                                        }
                                    });
                            snackbar.show();
                        }
                    }
                });

    }

    private void displayFavoritStatus() {
        if (preferences.isFavorite(building)) {
            favorite.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_favorite_fill));
        } else {
            favorite.setImageDrawable(this.getResources().getDrawable(R.drawable.ic_favorite_border));
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    public int convertDpToPixel(float dp) {
        Resources resources = getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) px;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        int height = convertDpToPixel(35);
        int width = convertDpToPixel(35);
        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.facebook_placeholder);
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

        Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(building.getLat()), Double.parseDouble(building.getLng()))).title(building.getName()));
        marker.setTag(building);
        marker.setIcon(BitmapDescriptorFactory.fromBitmap(smallMarker));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 10));

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        List<String> strings;

        public SectionsPagerAdapter(FragmentManager fm, List<String> strings) {
            super(fm);
            this.strings = strings;
        }

        @Override
        public Fragment getItem(int position) {
            return ImageFragment.newInstance(strings.get(position));
        }

        private String getPackage(int positon) {
            return strings.get(positon);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return strings.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }
}
