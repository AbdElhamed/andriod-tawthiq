package com.tawsiq.tawsiq.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tawsiq.tawsiq.Models.Building;
import com.tawsiq.tawsiq.R;
import com.tawsiq.tawsiq.activities.BuildingDetailsActivity;
import com.tawsiq.tawsiq.interfaces.BuildingAdapterCallback;
import com.tawsiq.tawsiq.utils.Preferences;
import com.tawsiq.tawsiq.utils.utils;

import java.util.ArrayList;
import java.util.List;

public class BuildingsAdapter extends RecyclerView.Adapter<BuildingsAdapter.ViewHolder> {

    private Preferences preferences;
    private Context context;
    private List<Building> items;

    public BuildingsAdapter(Context context) {
        items = new ArrayList<>();
        preferences = new Preferences(context);
        this.context = context;
    }


    public List<Building> getItems() {
        return items;
    }

    public void setItems(List<Building> items) {
        this.items = items;

    }

    public void delete(int po) {
        items.remove(po);
        this.notifyDataSetChanged();
    }

    public void addItem(Building item) {
        items.add(item);
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_aqar, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Building building = getItems().get(position);
        if (preferences.isFavorite(building)) {
            holder.favorite.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_favorite_fill));
        } else {
            holder.favorite.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_favorite_border));
        }

        switch (building.getType()) {
            case "1":
                holder.st.setImageDrawable(context.getResources().getDrawable(R.drawable.st1));
                break;
            case "2":
                holder.st.setImageDrawable(context.getResources().getDrawable(R.drawable.st2));
                break;
            case "3":
                holder.st.setImageDrawable(context.getResources().getDrawable(R.drawable.st3));
                break;
            case "4":
                holder.st.setImageDrawable(context.getResources().getDrawable(R.drawable.st4));
                break;
            default:
                holder.st.setVisibility(View.GONE);
                break;
        }

        holder.name.setText(utils.getAR(building.getName()));
        holder.price.setText(building.getPrice().concat(" ر.س."));
        holder.city.setText(preferences.getState(Integer.parseInt(building.getStateId())).toString());
        Picasso.get().load("https://tawthiq.com.sa/public/buildings/" + utils.getStringList(building.getPhoto()).get(0)).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        ImageView image, st;
        ImageView favorite;
        TextView name, price, city;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            st = itemView.findViewById(R.id.st);
            favorite = itemView.findViewById(R.id.favorite);
            name = itemView.findViewById(R.id.name);
            price = itemView.findViewById(R.id.price);
            city = itemView.findViewById(R.id.city);
            itemView.setOnClickListener(this);
            favorite.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            Building building = items.get(getAdapterPosition());
            if (v.getId() == R.id.favorite) {
                if (preferences.isFavorite(building)) {
                    preferences.removeFavorite(building);
                } else {
                    preferences.AddToFavorite(building);
                }
                notifyDataSetChanged();
                if (context instanceof BuildingAdapterCallback) {
                    BuildingAdapterCallback callback = (BuildingAdapterCallback) context;
                    callback.updateView();
                }
            } else {
                System.out.println(building.getName());
                Intent i = new Intent(context, BuildingDetailsActivity.class);
                i.putExtra(BuildingDetailsActivity.EXTRA_BUILDING, building);
                context.startActivity(i);


            }
        }

    }

}

