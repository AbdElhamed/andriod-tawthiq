package com.tawsiq.tawsiq.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.tawsiq.tawsiq.Models.Building;
import com.tawsiq.tawsiq.Models.State;
import com.tawsiq.tawsiq.Models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Preferences {

    private static final String DATA = "data";
    private static final String USER = "user";
    private static final String STATES = "sates";


    private SharedPreferences sharedPreferences;

    public Preferences(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

    }

    private void setData(String data) {
        sharedPreferences.edit().putString(DATA, data).apply();
    }

    private String getData() {
        return sharedPreferences.getString(DATA, "");
    }

    public void setUser(String user) {
        sharedPreferences.edit().putString(USER, user).apply();
    }

    public void setStates(String states) {
        sharedPreferences.edit().putString(STATES, states).apply();
    }

    public User getUser() {
        return new Gson().fromJson(sharedPreferences.getString(USER, ""), User.class);
    }

    public void clearUser() {
        sharedPreferences.edit().putString(USER, "").apply();
    }


    public List<Building> getAllFavorites() {
        Gson gson = new Gson();
        List<Building> buildings = new ArrayList<>();
        List<Building> bs = gson.fromJson(sharedPreferences.getString(DATA, ""), new TypeToken<List<Building>>() {
        }.getType());
        if (bs != null)
            buildings.addAll(bs);
        return buildings;
    }

    public List<State> getAllStates() {
        Gson gson = new Gson();
        List<State> buildings = new ArrayList<>();
        List<State> bs = gson.fromJson(sharedPreferences.getString(STATES, ""), new TypeToken<List<State>>() {
        }.getType());
        if (bs != null)
            buildings.addAll(bs);
        return buildings;
    }

    public boolean isFavorite(Building building) {
        Gson gson = new Gson();
        List<Building> buildings = new ArrayList<>();
        List<Building> bs = gson.fromJson(sharedPreferences.getString(DATA, ""), new TypeToken<List<Building>>() {
        }.getType());
        if (bs != null)
            buildings.addAll(bs);
        for (Building b : buildings) {
            if (b.getId() == building.getId()) {
                return true;
            }
        }
        return false;
    }

    public State getState(int id) {
        Gson gson = new Gson();
        List<State> buildings = new ArrayList<>();
        List<State> bs = gson.fromJson(sharedPreferences.getString(STATES, ""), new TypeToken<List<State>>() {
        }.getType());
        if (bs != null)
            buildings.addAll(bs);
        for (State b : buildings) {
            if (b.getId() == id) {
                return b;
            }
        }
        return null;
    }

    public void removeFavorite(Building building) {
        Gson gson = new Gson();
        List<Building> buildings = new ArrayList<>();
        List<Building> bs = gson.fromJson(sharedPreferences.getString(DATA, ""), new TypeToken<List<Building>>() {
        }.getType());
        if (bs != null)
            buildings.addAll(bs);

        for (int i = 0; i < buildings.size(); i++) {
            Building b = buildings.get(i);
            if (b.getId() == building.getId()) {
                buildings.remove(i);
            }
        }
        setFavorites(buildings);
    }

    private void setFavorites(List<Building> buildings) {
        sharedPreferences.edit().putString(DATA, new Gson().toJson(buildings)).apply();
    }

    public void AddToFavorite(Building building) {
        Gson gson = new Gson();
        List<Building> buildings = new ArrayList<>();
        List<Building> bs = gson.fromJson(sharedPreferences.getString(DATA, ""), new TypeToken<List<Building>>() {
        }.getType());
        if (bs != null)
            buildings.addAll(bs);
        buildings.add(building);
        setFavorites(buildings);
    }


}
