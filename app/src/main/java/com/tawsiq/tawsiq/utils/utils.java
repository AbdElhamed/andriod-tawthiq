package com.tawsiq.tawsiq.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class utils {
    public static String getAR(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            return jsonObject.getString("ar");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static   List<String> getStringList(String json) {
        List<String> list = new ArrayList<>();
        try {
            JSONArray jsonObject = new JSONArray(json);
            for (int i = 0 ; i < jsonObject.length();i++) {
                list.add(jsonObject.getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }
}
